/* eslint-disable no-undef */
describe("Web CoP app", () => {
  beforeEach(() => {
    cy.visit("https://web-newsletter-cop.web.app/");
  });

  it("displays newsletter and toolbar", () => {
    cy.get(".MuiToolbar-root").should("exist");

    // Displays newsletter tab content first
    cy.get(".MuiList-root").should("exist");

    cy.get(".MuiTab-root").should("have.length", 3);
    cy.get(".MuiTab-root").first().should("have.text", "Newsletter");
    cy.get(".MuiTab-root").last().should("have.text", "TBD");

    cy.get(".MuiTab-root").last().click();

    cy.get(".MuiList-root").should("not.exist");
    cy.get(".MuiTab-root").last().should("have.text", "TBD");
  });
});
