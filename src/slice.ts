import { createSlice } from "@reduxjs/toolkit";
import { NewsletterIssue, TalksData } from "./types";

enum loadingStates {
  NONE = "NONE",
  PENDING = "PENDING",
  SUCCESSFUL = "SUCCESSFUL",
  ERROR = "ERROR",
}

interface NewsletterSliceState {
  newsletterData: Array<NewsletterIssue>;
  selectedNewsletter: NewsletterIssue | null;
  selectedDate: string;
  status: loadingStates;
}

interface TalkSliceState {
  selectedYear: string;
  talkData: TalksData;
  status: loadingStates;
}

interface TalksAction {
  payload: TalksData;
}

interface WebCOPSliceState {
  newsletter: NewsletterSliceState;
  talks: TalkSliceState;
}

interface NewsletterAction {
  payload: {
    date: string;
    data: Array<NewsletterIssue>;
    newsletter: NewsletterIssue;
  };
}

export const webCOPSlice = createSlice({
  name: "webCOP",
  initialState: {
    newsletter: {
      newsletterData: [],
      selectedDate: "",
      selectedNewsletter: null,
      status: loadingStates.NONE,
    },
    talks: {
      selectedYear: new Date().getFullYear().toString(),
      talkData: {},
      status: loadingStates.NONE,
    },
  },
  reducers: {
    setNewsletterLoading: (state: WebCOPSliceState) => {
      state.newsletter.status = loadingStates.PENDING;
    },
    setNewsletterSuccess: (state: WebCOPSliceState, action: NewsletterAction) => {
      state.newsletter.status = loadingStates.SUCCESSFUL;
      state.newsletter.selectedNewsletter = action.payload.newsletter;
      state.newsletter.selectedDate = action.payload.date;
      state.newsletter.newsletterData = action.payload.data;
    },
    setNewsletterError: (state: WebCOPSliceState) => {
      state.newsletter.status = loadingStates.ERROR;
    },
    setTalksLoading: (state: WebCOPSliceState) => {
      state.talks.status = loadingStates.PENDING;
    },
    setTalksSuccess: (state: WebCOPSliceState, action: TalksAction) => {
      state.talks.status = loadingStates.SUCCESSFUL;
      state.talks.talkData = action.payload;
    },
    setTalksError: (state: WebCOPSliceState) => {
      state.talks.status = loadingStates.ERROR;
    },
    setSelectedDate: (state: WebCOPSliceState, action: { payload: string }) => {
      state.newsletter.selectedDate = action.payload;
    },
    setSelectedIssue: (
      state: WebCOPSliceState,
      action: {
        payload: {
          newsletter: NewsletterIssue;
        };
      }
    ) => {
      state.newsletter.selectedNewsletter = action.payload.newsletter;
      state.newsletter.selectedDate = action.payload.newsletter.date;
    },
    setSelectedYear: (state: WebCOPSliceState, action: { payload: string }) => {
      state.talks.selectedYear = action.payload;
    },
  },
});

export const {
  setSelectedDate,
  setSelectedIssue,
  setNewsletterLoading,
  setNewsletterError,
  setNewsletterSuccess,
  setSelectedYear,
  setTalksLoading,
  setTalksSuccess,
  setTalksError
} = webCOPSlice.actions;

// Newsletter selectors
export const getIsDataLoaded = (state: WebCOPSliceState) =>
  state.newsletter.status === loadingStates.SUCCESSFUL;
export const getNewsletterData = (state: WebCOPSliceState) =>
  state.newsletter.newsletterData;
export const getSelectedDate = (state: WebCOPSliceState) =>
  state.newsletter.selectedDate;
export const getSelectedNewsletter = (state: WebCOPSliceState) =>
  state.newsletter.selectedNewsletter;

export const findSelectedNewsletter = (state: WebCOPSliceState) => {
  const selectedDate = getSelectedDate(state);
  const newsletterData = getNewsletterData(state);

  return newsletterData.find((newsletter) => newsletter.date === selectedDate);
};

// Talks selectors
export const getIsTalkDataLoaded = (state: WebCOPSliceState) =>
  state.talks.status === loadingStates.SUCCESSFUL;
export const getSelectedYear = (state: WebCOPSliceState) =>
  state.talks.selectedYear;
export const getTalksData = (state: WebCOPSliceState) =>
  state.talks.talkData;

export default webCOPSlice.reducer;
