import { onValue, ref } from "firebase/database";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { CircularProgress } from "@mui/material";
import { db } from "../../api/firebase";
import { setNewsletterLoading, setNewsletterSuccess } from "../../slice";

function NewsletterLoader() {
  const dispatch = useDispatch();

  useEffect(() => {
    const loadNewsletters = () => {
      dispatch(setNewsletterLoading);
      const query = ref(db, "newsletters");

      return onValue(query, (snapshot) => {
        if (snapshot.exists()) {
          const data = snapshot.val();

          const newsletterData = data;
          const date = newsletterData[0].date;

          const loadedData = {
            date,
            data: newsletterData,
            newsletter: newsletterData[0],
          };
          dispatch(setNewsletterSuccess(loadedData));
        }
      });
    };

    loadNewsletters();
  }, [dispatch]);

  return (
    <div>
      <CircularProgress />
    </div>
  );
}

export default NewsletterLoader;
