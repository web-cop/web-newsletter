import React from "react";
import { useSelector } from "react-redux";
import Link from "@mui/material/Link";
import { findSelectedNewsletter, getIsDataLoaded } from "../../slice";
import { NewsletterIssue } from "../../types";
import NewsletterLoader from "./NewsletterLoader";

const showNewsletterData = (newsletter: NewsletterIssue) => {
  const sections = newsletter.sections;

  return (
    <>
      <h1>{newsletter.issue}</h1>
      <>
        {sections.map((section, sectionIndex) => {
          return (
            <div key={sectionIndex}>
              <h2>{section.heading}</h2>
              {section.links.map((link, linkIndex) => {
                return (
                  <div key={linkIndex}>
                    <Link href={link.url} target="_blank">
                      {link.text}
                    </Link>
                  </div>
                );
              })}
            </div>
          );
        })}
      </>
    </>
  );
};

function Newsletter() {
  const newsletter = useSelector(findSelectedNewsletter);
  const isDataLoaded = useSelector(getIsDataLoaded);

  return (
    <>
      {!isDataLoaded && <NewsletterLoader />}
      {isDataLoaded && newsletter && showNewsletterData(newsletter)}
    </>
  );
}

export default Newsletter;
