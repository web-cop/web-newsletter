import React from "react";
import { useSelector } from "react-redux";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import {
  getNewsletterData,
  getSelectedDate,
  setSelectedDate,
} from "../../slice";
import Newsletter from "./Newsletter";
import SideDrawer from "../shared/SideDrawer";

function NewsletterLayout() {
  const selectedDate = useSelector(getSelectedDate);
  const newsletterData = useSelector(getNewsletterData);

  const dateListItems = newsletterData.map((newsletter) => {
    return newsletter.date;
  });

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <SideDrawer
        listItems={dateListItems}
        selectedComparisonItem={selectedDate}
        onSelectedListItem={setSelectedDate}
      />
      <Box component="main" sx={{ p: 2 }}>
        <Newsletter />
      </Box>
    </Box>
  );
}

export default NewsletterLayout;
