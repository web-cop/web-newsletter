import React from "react";
import { Avatar, Grid } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Link from "@mui/material/Link";
import Typography from "@mui/material/Typography";
import { getProfileImg } from "./contributors/utils";
import { Talk } from "../../types";

export default function TalkCard(props: { talk: Talk }) {
  const { date, presenter, talkTitle, url } = props.talk;

  return (
    <>
      <Card sx={{ minWidth: 275, height: '100%' }}>
        <CardContent>
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            <Grid container justifyContent="flex-start" spacing={3}>
              <Grid item xs={10}>
                {date} | {presenter}
                <Typography variant="h5" component="div">
                  <Link href={url} target="_blank" variant="inherit">
                    {talkTitle}
                  </Link>
                </Typography>
              </Grid>
              <Grid item xs={2}><Avatar alt={presenter} src={getProfileImg(presenter)} /></Grid>
            </Grid>
          </Typography>
        </CardContent>
      </Card>
    </>
  );
}
