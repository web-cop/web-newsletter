import React from "react";
import { useSelector } from "react-redux";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import { getSelectedYear, setSelectedYear, getTalksData } from "../../slice";
import SideDrawer from "../shared/SideDrawer";
import Talks from "./Talks";

function TalksLayout() {
  const selectedYear = useSelector(getSelectedYear);
  const talkData = useSelector(getTalksData);

  const yearListItems = Object.entries(talkData).map(([year]) => {
    return year;
  }).sort().reverse();

  return (
    <Box sx={{ display: "flex", padding: "38px" }}>
      <CssBaseline />
      <SideDrawer
        listItems={yearListItems}
        selectedComparisonItem={selectedYear}
        onSelectedListItem={setSelectedYear}
      />
      <Talks />
      
    </Box>
  );
}

export default TalksLayout;
