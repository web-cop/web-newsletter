import { onValue, ref } from "firebase/database";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { CircularProgress } from "@mui/material";
import { db } from "../../api/firebase";
import { setTalksLoading, setTalksSuccess } from "../../slice";

function TalksLoader() {
  const dispatch = useDispatch();

  useEffect(() => {
    const loadTalks = () => {
      dispatch(setTalksLoading);
      const query = ref(db, "talks");

      return onValue(query, (snapshot) => {
        if (snapshot.exists()) {
          const data = snapshot.val();
          dispatch(setTalksSuccess(data));
        }
      });
    };

    loadTalks();
  }, [dispatch]);

  return (
    <div>
      <CircularProgress />
    </div>
  );
}

export default TalksLoader;
