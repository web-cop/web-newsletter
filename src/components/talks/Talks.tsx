import React from "react";
import { useSelector } from "react-redux";
import Grid from "@mui/material/Grid";
import TalkCard from "./TalkCard";
import { getIsTalkDataLoaded, getSelectedYear, getTalksData } from "../../slice";
import TalksLoader from "./TalksLoader";
import { Talk } from "../../types";

const showTalksData = (talks: Array<Talk>) => {
    return (<Grid container justifyContent="flex-start" spacing={3}>
        {talks.map((talk: Talk, index: number) => {
          return (
            <Grid item key={index} xs={4}>
              <TalkCard talk={talk} />
            </Grid>
          );
        })}
      </Grid>);
};

function Talks() {
  const isDataLoaded = useSelector(getIsTalkDataLoaded);
  const selectedYear = useSelector(getSelectedYear);
  const talkData = useSelector(getTalksData);

  const yearTalks = talkData[selectedYear];

  return (
    <>
      {!isDataLoaded && <TalksLoader />}
      {isDataLoaded && yearTalks && showTalksData(yearTalks)}
    </>
  );
}

export default Talks;
