import caitlinSalt from './caitlinSalt.jpg';
import deanKerr from './deanKerr.jpg';
import harryHartleySaunders from './harryHartleySaunders.jpg';
import harryLaw from './harryLaw.jpg';
import jamieMcDonald from './jamieMcDonald.jpg';
import jevonBeckles from './jevonBeckles.jpg';
import kieranEllis from './kieranEllis.jpg';
import odedSharon from './odedSharon.jpg';
import robatWilliams from './robatWilliams.jpg';
import samGladstone from './sGladstone.jpg';
import tomPearson from './tomPearson.jpg';
import valiFlorescu from './valiFlorescu.jpg';
import willFerguson from './willFerguson.jpg';

export const getProfileImg = (name: string) => {
    switch (name) {
        case 'Caitlin Salt':
            return caitlinSalt;
        case 'Dean Kerr':
            return deanKerr;
        case 'Harry Hartley-Saunders':
            return harryHartleySaunders;
        case 'Harry Law':
            return harryLaw;
        case 'Kieran Ellis':
            return kieranEllis;
        case 'Jamie Macdonald':
            return jamieMcDonald;
        case 'Jevon Beckles':
            return jevonBeckles;
        case 'Oded Sharon':
            return odedSharon;
        case 'Robat Williams':
            return robatWilliams;
        case 'Sam Gladstone':
            return samGladstone;
        case 'Tom Pearson':
            return tomPearson;
        case 'Vali Florescu':
            return valiFlorescu;
        case 'Will Ferguson':
            return willFerguson;
        default:
            // Return string so that profile img uses default first letter of presenter name
            return 'noImgAvailable';
    }
}