import React from "react";
import Box from "@mui/material/Box";

function Tbd() {
  return (
    <Box sx={{ display: "flex" }}>
      <Box component="main" sx={{ p: 3 }}>
        <>TBD</>
      </Box>
    </Box>
  );
}

export default Tbd;
