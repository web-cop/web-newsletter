import React from "react";
import { useDispatch } from "react-redux";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import { DRAWER_WIDTH } from "../../constants";
import { AnyAction } from "@reduxjs/toolkit";

interface SideDrawerProps {
  listItems: Array<string>;
  selectedComparisonItem: string;
  onSelectedListItem: (selectedListItem: string) => AnyAction;
}

function SideDrawer({
  listItems,
  selectedComparisonItem,
  onSelectedListItem,
}: SideDrawerProps) {
  const dispatch = useDispatch();

  return (
    <Drawer
      variant="permanent"
      sx={{
        width: DRAWER_WIDTH,
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: {
          width: DRAWER_WIDTH,
          boxSizing: "border-box",
        },
      }}
    >
      <Toolbar />
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <List>
          {listItems.map((listItem) => {
            return (
              <ListItem key={listItem} disablePadding>
                <ListItemButton
                  onClick={() => dispatch(onSelectedListItem(listItem))}
                  selected={selectedComparisonItem === listItem}
                >
                  <ListItemText primary={listItem} />
                </ListItemButton>
              </ListItem>
            );
          })}
        </List>
      </Box>
    </Drawer>
  );
}

export default SideDrawer;
