import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAT-0QU1GYSnJ-YSB_o736ojlSPFpNjdIU",
  authDomain: "web-newsletter-cop.firebaseapp.com",
  databaseURL:
    "https://web-newsletter-cop-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "web-newsletter-cop",
  storageBucket: "web-newsletter-cop.appspot.com",
  messagingSenderId: "281156118399",
  appId: "1:281156118399:web:a506cfbd989a3e96fa9c4a",
  measurementId: "G-PNMDL9QWPY",
};

const app = initializeApp(firebaseConfig);
export const db = getDatabase(app);
