interface NewsletterLink {
    text: string;
    url: string;
}

interface NewsletterSection {
    heading: string;
    links: Array<NewsletterLink>;
}

export interface NewsletterIssue {
    date: string;
    issue: string;
    sections: Array<NewsletterSection>;
}

export interface Talk {
    date: string;
    presenter: string;
    talkTitle?: string;
    url: string;
}

export interface TalksData {
    [key: string]: Array<Talk>;
}