import React from "react";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import { render } from "@testing-library/react";
import App from "./App";
import { webCOPSlice } from "./slice";

export function createTestStore() {
  return configureStore(webCOPSlice);
}

export function renderWithProvider(children: JSX.Element) {
  return render(<Provider store={createTestStore()}>{children}</Provider>);
}

test("renders learn react link", () => {
  const { getByText } = renderWithProvider(<App />);

  // @ts-expect-error FIX ME NEEDED HERE?
  expect(getByText(/Web Community of Practice/i)).toBeInTheDocument();
});
